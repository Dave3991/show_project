FROM php:7.4-apache

ENV DEBIAN_FRONTEND=noninteractive

COPY ./app /var/www/html
WORKDIR /var/www/html